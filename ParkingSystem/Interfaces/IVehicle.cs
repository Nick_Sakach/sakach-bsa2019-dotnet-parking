﻿using System;
using static ParkingSystem.Classes.Configuration;

namespace ParkingSystem.Interfaces
{
    interface IVehicle
    {
        Guid Guid { get; }

        string Name { get; set; }

        VehicleTypes Type { get; }
    }
}
