﻿using System;
using ParkingSystem.Interfaces;

namespace ParkingSystem.Interfaces
{
    interface IParkingSlot
    {
        IWallet Wallet { get; }

        IVehicle Vehicle { get; }

        uint UnpaidCounter { get; }

        bool IsVehicleBlocked { get; }

        bool ChargeCost(out double outcome);
    }
}
