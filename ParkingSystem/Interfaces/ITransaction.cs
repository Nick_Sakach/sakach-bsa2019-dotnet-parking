﻿using System;

namespace ParkingSystem.Interfaces
{
    interface ITransaction
    {
        DateTime Time { get; }

        string Description { get; }

        string Source { get; }
    }
}
