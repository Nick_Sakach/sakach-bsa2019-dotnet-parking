﻿using System;

namespace ParkingSystem.Interfaces
{
    interface IWallet
    {
        double Amount { get; }

        Guid Guid { get; }

        void AddMoney(double CostAmount);

        bool TryChargeMoney(double CostAmount);
    }
}
