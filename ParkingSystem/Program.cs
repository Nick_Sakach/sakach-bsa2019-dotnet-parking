﻿using System;
using System.Threading;
using ParkingSystem.Classes;

namespace ParkingSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting...");

            var parking = Parking.GetParkingInstance();
            TransactionLogger Logger = TransactionLogger.GetLoggerInstance();
            TimerCallback recalculateCosts = new TimerCallback(parking.CollectCosts);
            Timer Broker = new Timer(recalculateCosts, null, 0, 5000);

            Console.WriteLine("Welcome to Parking System management tool! ");
            Console.WriteLine();
            var continueFlow = true;
            while (continueFlow)
            {
                continueFlow = MainMenu(Logger);
            }
            Console.Write("Thanks fo using our sysytem! Press any key to quit. ");
            Console.ReadLine();
        }

        static bool MainMenu(TransactionLogger logger, bool ClearConsole = true)
        {
            var parking = Parking.GetParkingInstance();            
            Console.WriteLine();
            Console.WriteLine("#####  MAIN MENU  #####");
            Console.WriteLine();
            Console.WriteLine("Select SubMenu (input its number/letter and press ENTER):");
            Console.WriteLine("0. Show system`s amount (balance)");
            Console.WriteLine("1. Show system`s income in last minute period");
            Console.WriteLine("2. Output current count of free/used places");
            Console.WriteLine("3. Output current parking slots info");
            Console.WriteLine("4. Add Vehicle");
            Console.WriteLine("5. Remove Vehicle");
            Console.WriteLine("6. Add some amount of money to specific wallet");
            Console.WriteLine("7. Get transactions for last minute");
            Console.WriteLine("8. Get transaction info from TransactionLog.txt");
            Console.WriteLine("C. Edit configuration");
            Console.WriteLine("H. Help Menu (some basic info)");
            Console.WriteLine("Q. Quit");
            Console.WriteLine();
            var variant = Console.ReadLine();
            if (ClearConsole) Console.Clear();

            switch (variant.ToLower())
            {
                case "0": parking.ShowAmount(); break;
                case "1": parking.ShowIncomeSum((DateTime.Now.AddMinutes(-1))); break;
                case "2": parking.ShowSlotStateCounts(); break;
                case "3": parking.ShowParkingSlotsDetails(); break;
                case "4": parking.AddVehicle(); break;
                case "5": parking.RemoveVehicle(); break;
                case "6": parking.IncreaseBalanceAmount(); break;
                case "7": logger.ReadTransactions(DateTime.Now.AddMinutes(-1)); break;
                case "8": logger.ReadLog(); break;
                case "c": ComingSoon(); break;
                case "h": ComingSoon(); break;
                case "q": return false;
                default: UnrecognizedInput(); break;

            }
            return true;
        }

        static void UnrecognizedInput() => Console.WriteLine("\n"+
            "We haven't matched your input with current functionality. \n"
            + "Please check your input and try again. \n");

        static void ComingSoon() => 
            Console.WriteLine("\n" + "Coming soon...\n");
    }
}
