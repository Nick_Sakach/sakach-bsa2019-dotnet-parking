﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ParkingSystem.Interfaces;

namespace ParkingSystem.Classes
{
    class TransactionLogger
    {
        private static readonly string LogPath = $"..\\TransactionLog {DateTime.Now.ToString("yyyy_MM_dd HH_mm_ss")}.txt";
        private static TransactionLogger logger;

        public static Dictionary<DateTime, string> Transactions = new Dictionary<DateTime, string>();
        private TransactionLogger() { }

        public static TransactionLogger GetLoggerInstance()
        {
            if (logger == null)
            {
                logger = new TransactionLogger();
            }
            return logger;
        }

        public void WriteToLog(ITransaction transaction)
        {
            try
            {
                Transactions.Add(DateTime.Now, transaction.ToString());
                using (FileStream fstream = new FileStream(LogPath, FileMode.OpenOrCreate))
                {
                    byte[] logText = Encoding.Default.GetBytes(transaction.ToString() + "\n\n");

                    fstream.Seek(0, SeekOrigin.End);
                    fstream.Write(logText);

                }
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("\n" + "Logging error: File is not accesible! Please check permissions or run this program as Admin.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n" + "WriteToLog error: " + ex.Message);
            }
        }

        public void ReadLog()
        {
            try
            {
                using (FileStream fstream = new FileStream(LogPath, FileMode.OpenOrCreate))
                {
                    fstream.Seek(0, SeekOrigin.Begin);
                    var output = new byte[fstream.Length];

                    fstream.Read(output, 0, output.Length);
                    Console.WriteLine("Transation Log file info: \n\n" + Encoding.Default.GetString(output));
                }
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("\n" + "Logging error: File is not accesible! Please check permissions or run this program as Admin.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("\n" + "WriteToLog error: " + ex.Message);
            }
        }

        public void ReadTransactions(DateTime timeFrom)
        {
            Console.WriteLine("Transaction list for specified amount of time: \n");
            var listToOutput = Transactions.Where(x => x.Key > timeFrom).Select(x => x.Value).ToList();

            foreach (var item in listToOutput)
                Console.WriteLine(item + "\n");
        }
    }

}
