﻿using ParkingSystem.Interfaces;
using System;
using static ParkingSystem.Classes.Configuration;

namespace ParkingSystem.Classes
{
    class Vehicle : IVehicle
    {
        public Guid Guid { get; private set; }

        public string Name { get; set; }

        public VehicleTypes Type { get; private set; }

        public Vehicle(VehicleTypes type)
        {
            this.Guid = Guid.NewGuid();
            this.Name = "";
            this.Type = type;
        }
    }
}
