﻿using System;
using ParkingSystem.Interfaces;
using static ParkingSystem.Classes.Configuration;

namespace ParkingSystem.Classes
{
    class ParkingSlot : IParkingSlot
    {
        public IVehicle Vehicle { get; private set; }

        public IWallet Wallet { get; private set; }

        public uint UnpaidCounter { get; private set; }

        public bool IsVehicleBlocked { get; private set; }

        private double multiplier;

        public bool ChargeCost(out double outcome)
        {
            double amount = ChargeAmount * multiplier 
                + ChargeAmount * multiplier * UnpaidCounter * OverusedCostTaxRate;

            bool isCostCharged = Wallet.TryChargeMoney(amount);
            if (!isCostCharged)
            {
                outcome = 0;
                UnpaidCounter++;
                IsVehicleBlocked = true;
            }
            else
            {
                outcome = amount;
                UnpaidCounter = 0;
                IsVehicleBlocked = false;
            }
            return isCostCharged;
        }

        public ParkingSlot(VehicleTypes type)
        {
            this.Wallet = new BillingAccount(Configuration.DefaultCostStartAmount);
            this.Vehicle = new Vehicle(type);
            this.IsVehicleBlocked = true;
            this.UnpaidCounter = 0;
            this.multiplier = GetCostMultiplier(type);
        }

        public ParkingSlot(VehicleTypes type, double startAmount)
        {
            this.Wallet = new BillingAccount(startAmount);
            this.Vehicle = new Vehicle(type);
            this.IsVehicleBlocked = true;
            this.UnpaidCounter = 0;
            this.multiplier = GetCostMultiplier(type);
        }

        private double GetCostMultiplier(VehicleTypes type)
        {
            double result = 1;
            switch (type)
            {
                case VehicleTypes.Bike: result = ChargeMultipliers.Bike; break;
                case VehicleTypes.Truck: result = ChargeMultipliers.Truck; break;
                case VehicleTypes.Bus: result = ChargeMultipliers.Bus; break;
                case VehicleTypes.Passenger: result = ChargeMultipliers.Passenger; break;
                default: multiplier = 1; break;
            }
            return result;
        }
    }
}
