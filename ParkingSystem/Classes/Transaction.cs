﻿using ParkingSystem.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace ParkingSystem.Classes
{
    class Transaction : ITransaction
    {
        public DateTime Time { get; }
        public string Description { get; }
        public string Source { get; }
        public override string ToString()
            => this.Time + " -> " + this.Source + ": " + this.Description;

        public Transaction(string tranSource, string tranDescr)
        {
            this.Time = DateTime.Now;
            this.Source = tranSource;
            this.Description = tranDescr;
        }
    }
}
