﻿using System;

namespace ParkingSystem.Classes
{
    public static class Configuration
    {
        public enum VehicleTypes
        {
            Bus,
            Bike,
            Truck,
            Passenger
        }

        public static class ChargeMultipliers
        {
            public static double Bus = 3.5;
            public static double Bike = 1;
            public static double Truck = 5;
            public static double Passenger = 2;
        }

        public static double ChargeAmount = 1;

        public static double OverusedCostTaxRate = 2.5;

        public static int ParkingSlotsCont = 10;

        public static double DefaultCostStartAmount = 10;

        public static string СurrencyName = "UAH";

    }
    
}
