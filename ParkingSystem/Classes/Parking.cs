﻿using System;
using System.Text;
using System.Linq;
using ParkingSystem.Interfaces;
using static ParkingSystem.Classes.Configuration;
using System.Collections.Generic;

namespace ParkingSystem.Classes
{
    class Parking
    {
        private static Parking parking;
        private static TransactionLogger Logger = TransactionLogger.GetLoggerInstance();

        public Dictionary<DateTime, double> Incomes = new Dictionary<DateTime, double>();

        public IWallet Budget { get; private set; }

        public IParkingSlot[] Slots { get; set; }
        private Parking()
        {
            Slots = new ParkingSlot[ParkingSlotsCont];
            Slots[0] = new ParkingSlot(VehicleTypes.Truck, 25000);
            Budget = new BillingAccount(0);
        }

        public static Parking GetParkingInstance()
        {
            if (parking == null)
            {
                parking = new Parking();
            }
            return parking;
        }

        public void ShowAmount()
        {
            Console.WriteLine($"Current Parking's balance is {parking.Budget.Amount} {Configuration.СurrencyName}");
        }

        public void ShowIncomeSum(DateTime timeFrom)
        {
            var amount = Incomes.Where(x => x.Key > timeFrom).Select(x => x.Value).Sum();
            Console.WriteLine("For mentioned period cost income was {0}{1}", amount, Configuration.СurrencyName);
        }

        public void ShowSlotStateCounts()
        {
            var totalCount = Slots.Length;
            var free = Slots.Where(x => x == null).Count();
            Console.WriteLine($"\nTotal Parking slots: {totalCount}");
            Console.WriteLine($"Free Parking slots: {free}");
            Console.WriteLine($"Used Parking slots: {totalCount - free}");
        }

        public void ShowParkingSlotsDetails()
        {
            for (int i = 0; i < Slots.Length; i++)
            {
                Console.Write($"Slot {i} info:");
                var slotInfo = Slots[i] == null ? " Empty. \n" : GetSlotInfo(Slots[i]);
                Console.Write(slotInfo + "\n");
            }
        }

        public string GetSlotInfo(IParkingSlot slot)
        {
            StringBuilder result = new StringBuilder();
            result.AppendLine();
            result.AppendLine("Car info:");
            result.AppendLine($"Type: {slot.Vehicle.Type.ToString()}");
            result.AppendLine($"GUID: {slot.Vehicle.Guid}");
            result.AppendLine($"Payer wallet's GUID: {slot.Wallet.Guid}");
            result.AppendLine($"Payer's amount: {slot.Wallet.Amount}");
            result.AppendLine($"Is car blocked: {slot.IsVehicleBlocked}");
            if (slot.UnpaidCounter > 0)
                result.AppendLine($"Times costs weren't paid: {slot.UnpaidCounter}");
            return result.ToString();
        }

        public void AddVehicle()
        {
            Console.WriteLine("4. Add Vehicle \n");
            if (Slots.Where(x => x == null).Any() == false)
            {
                Console.WriteLine("All parking lots are already used!");
                return;
            }
            Console.WriteLine("Select creatable's car type:");
            Console.WriteLine("1. Passenger");
            Console.WriteLine("2. Bike");
            Console.WriteLine("3. Bus");
            Console.WriteLine("4. Truck");
            var input = Console.ReadLine();

            VehicleTypes type;
            switch (input)
            {
                case "1": type = VehicleTypes.Passenger; break;
                case "2": type = VehicleTypes.Bike; break;
                case "3": type = VehicleTypes.Bus; break;
                case "4": type = VehicleTypes.Truck; break;
                default: Console.WriteLine("Incorrect type! Terminating..."); return;
            }

            Console.WriteLine("Input numeric amount of costs avaliable, otherwise default amount will be used");
            IParkingSlot slot;
            slot = double.TryParse(Console.ReadLine(), out double amount) ? new ParkingSlot(type, amount) : new ParkingSlot(type);
            for (int i=0; i < Slots.Length; i++)
            {
                if (Slots[i] == null)
                {
                    Slots[i] = slot;
                    break;
                }
            }
            Console.WriteLine("\n" + $"Vehicle {slot.Vehicle.Guid} ({slot.Vehicle.Type.ToString()}) with amount {slot.Wallet.Amount}{СurrencyName} created!");
        }

        public void RemoveVehicle()
        {
            Console.WriteLine("5. Remove Vehicle \n");
            Console.WriteLine("Please input car's GUID below: ");
            var input = Console.ReadLine();
            Console.WriteLine();
            var slot = Slots.Where(x => x?.Vehicle?.Guid.ToString() == input).FirstOrDefault();
            if(slot == null)
            {
                Console.WriteLine("Vehicle with such GUID is not found. Check your input");
            }
            else
            {
                if (slot.IsVehicleBlocked)
                {
                    Console.WriteLine("Vehicle with such GUID is currently blocked! Please increase corresponding wallet's amount to pay taxes and unblock it.");
                    Console.WriteLine("(This time you can just add any amount of money you want for free, but next time be more attentive to your balance)");
                }
                else
                {
                    Console.WriteLine($"{slot.Vehicle.Type.ToString()} {slot.Vehicle.Guid} will be deleted. Do you want to proceed? (type 'y' to delete)");

                    if (Console.ReadLine().ToLower() == "y")
                    {
                        for (int i = 0; i < Slots.Length; i++)
                        {
                            if (Slots[i].Vehicle.Guid.ToString() == input)
                            {
                                Slots[i] = null;
                                break;
                            }
                        }
                        Console.WriteLine("Done.");
                    }
                    else
                    {
                        Console.WriteLine("Oeration Cancelled.");
                    }
                }
            }
        }

        public void IncreaseBalanceAmount()
        {
            Console.WriteLine("6. Add some amount of money to specific wallet \n");
            Console.WriteLine("Please input wallet's GUID below: ");
            var input = Console.ReadLine();
            var wallet = Slots.Where(x => x?.Wallet?.Guid.ToString() == input).Select(x => x?.Wallet).FirstOrDefault();
            if (wallet == null)
            {
                Console.WriteLine("\n" + "Wallet with such GUID wasn't found! please check your input" + "\n");
            }
            else
            {
                Console.WriteLine("Please input amount for increasement below: ");
                input = Console.ReadLine();
                if (double.TryParse(input, out double costValue) && costValue > 0)
                {
                    wallet.AddMoney(costValue);
                    Console.WriteLine("Done.");
                }
                else
                {
                    Console.WriteLine("Incorrect value! Please use numeric value and check that it's positive!");
                }
            }
        }

        public void CollectCosts(object _)
        {
            bool isCostCharged;
            foreach (var item in Slots)
            {
                try
                {
                    if (item != null)
                    {
                        isCostCharged = item.ChargeCost(out double outcome);

                        var details = isCostCharged ? $"Rent {outcome}{Configuration.СurrencyName} was paid successfully."
                            : $"Rent wasn't paid {item.UnpaidCounter} time(s). Car is currently blocked";

                        var transaction = new Transaction($"Vehicle {item.Vehicle.Guid} ({item.Vehicle.Type.ToString()})", details);
                        Logger.WriteToLog(transaction);

                        if (outcome > 0)
                        {
                            parking.Budget.AddMoney(outcome);
                            Incomes.Add(DateTime.Now, outcome);
                            transaction = new Transaction("Parking", $"Amount {outcome}{Configuration.СurrencyName} was transferred to budget.");
                            Logger.WriteToLog(transaction);
                        }
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Collect Costs error: " + ex.Message);
                }                
            }
        }
    }
}
